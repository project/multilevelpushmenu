(function ($) {

  Drupal.multilevelpushmenu = {
    menu_created: false,
    responsive_width: 485,
    scrollPosition: 0,
    pushing: [],

    create: function () {
      var menu = $('.multilevelpushmenu');
      var wrapper = $('.menu-block-wrapper', menu);
      var menu_button = $('.multilevelpushmenu-link');

      if (Drupal.multilevelpushmenu.menu_created) {
        if ($(window).width() >= Drupal.multilevelpushmenu.responsive_width) {
          menu.hide();
          menu_button.hide();
        }
        else {
          menu.multilevelpushmenu('redraw');
          menu.show();
          menu_button.show();
        }
        return;
      }

      menu.multilevelpushmenu({
        container: $('.multilevelpushmenu'),
        containersToPush: Drupal.multilevelpushmenu.pushing,
        menuWidth: '100%',
        menuHeight: $(document).height(),
        collapsed: true,
        mode: 'overlap',
        preventItemClick: true,
        fullCollape: false,
        preventGroupItemClick: true,
        swipe: 'touchscreen'
      });

      menu_button.click(function() {
        console.log("clicked");
        if (menu.hasClass('expanded')) {
          console.log("is closing");
          $('html, body').animate({
            scrollTop: Drupal.multilevelpushmenu.scrollPosition
          }, 220, 'swing');
          menu.multilevelpushmenu('collapse');
          menu.removeClass('expanded');
          $('.multilevelpushmenu_wrapper', menu).fadeOut().css('display', 'none');
          for (var i = 0; i < Drupal.multilevelpushmenu.pushing.length; i++) {
            pushing[i].removeClass('multilevelpushmenu-pushed');
          }
          $(this).removeClass('exit');
        }
        else {
          console.log("is opening");
          Drupal.multilevelpushmenu.scrollPosition = parseInt(document.documentElement.scrollTop || document.body.scrollTop);
          console.log(Drupal.multilevelpushmenu.scrollPosition);
          menu.multilevelpushmenu('expand');
          menu.addClass('expanded');
          $('.multilevelpushmenu_wrapper', menu).fadeIn()
            .css('display', 'block');
          for (var i = 0; i < Drupal.multilevelpushmenu.pushing.length; i++) {
            pushing[i].addClass('multilevelpushmenu-pushed');
          }
          $(this).addClass('exit');
          $('html, body').animate({
            scrollTop: 0
          }, 220, 'swing');

        }
        return false;
      });

      Drupal.multilevelpushmenu.menu_created = true;
    }
  }

  Drupal.behaviors.multilevelpushmenu = {
    attach: function (context, settings) {
      Drupal.multilevelpushmenu.create();
    }
  }

  $(window).resize(function() {
    Drupal.multilevelpushmenu.create();
  });
})(jQuery);
